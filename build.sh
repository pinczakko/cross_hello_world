#!/bin/bash

#
# Script to build the application in the platform where this script is run.
#

BOOST_ROOT="~/boost_cpp"
echo "BOOST_ROOT="$BOOST_ROOT
export BOOST_ROOT

rm -rvf build
mkdir -p build

cd build
#cmake -G"Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=./toolchain-x86_64-mingw-w64.cmake ..
cmake -G"Unix Makefiles" ..
make VERBOSE=1

