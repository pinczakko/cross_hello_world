#define BOOST_THREAD_VERSION 4
#define BOOST_THREAD_PROVIDES_FUTURE
#define BOOST_THREAD_USES_DATETIME

#include <future>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include "asyncTest.h"

using namespace boost::filesystem;
using namespace std;

void workerFunc()
{
    boost::posix_time::seconds workTime(3);

    std::cout << "Worker: running" << std::endl;

    // Pretend to do something useful...
    boost::this_thread::sleep(workTime);

    std::cout << "Worker: finished" << std::endl;
}

void called_from_async() 
{
  std::cout << "Async call" << std::endl;
}

int bind_test(int param1, float param2) 
{
  	std::cout << "param1 = " << param1  << std::endl;
  	std::cout << "param2 = " <<  param2 << std::endl;
	
	return 0;
}

void lambda_test() {
 	std::cout << "Testing lambda .." << std::endl;
	
	auto lambda = []() { cout << "Code within a lambda expression" << endl; };
    lambda();
};
		  

int main (int argc, char** argv)
{
    std::cout << "Hello World C++\n";

    if (argc < 2) {
        std::cout << "Usage: "<< argv[0]  <<" path\n" ;
        return 1;
    }

	path p(argv[1]); 

    if (exists(p))    // does p actually exist?
    {
        if (is_regular_file(p))        // is p a regular file?
            cout << p << " size is " << file_size(p) << '\n';

        else if (is_directory(p))      // is p a directory?
            cout << p << "is a directory\n";

        else
            cout << p << "exists, but is neither a regular file nor a directory\n";
    }
    else
	{
        cout << p << "does not exist\n";
	}

    std::cout << "main: startup" << std::endl;

    boost::thread workerThread(workerFunc);

	//called_from_async launched in a separate thread if possible
	std::future<void> result( std::async(called_from_async));
	
	lambda_test();

	boost::bind(bind_test, 2, 3.14)();

    std::cout << "main: waiting for thread" << std::endl;

    workerThread.join();

	result.get();

	pcz::AsyncTest at; 
	at.WaitForThread();

    std::cout << "main: done" << std::endl;

    return 0;
}

