#!/bin/bash

#_architectures="x86_64-w64-mingw32 i686-w64-mingw32"
_architectures="x86_64-w64-mingw32"

rm -rvf build-*

#cd build
#cmake -G"Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=./toolchain-x86_64-mingw-w64.cmake ..
#make VERBOSE=1

#cd - 

for _arch in ${_architectures}; do
	mkdir -p build-${_arch} && pushd build-${_arch}
	CMAKE_INCLUDE_PATH="/usr/"${_arch}"/include"
	echo "CMAKE_INCLUDE_PATH = "${CMAKE_INCLUDE_PATH}
	export CMAKE_INCLUDE_PATH
  	##${_arch}-cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
  	${_arch}-cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
	make VERBOSE=1
	popd
done	

