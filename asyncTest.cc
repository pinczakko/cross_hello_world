#define BOOST_THREAD_VERSION 4
#define BOOST_THREAD_PROVIDES_FUTURE

#include "asyncTest.h"
#include <iostream>
//#include <future>

#include <boost/thread.hpp>
#include <boost/thread/future.hpp>

namespace pcz {

	void AsyncTest::testFutureFunc(){
		std::cout << "Inside function testFutureFunc" << std::endl;
		std::cout << "data = " << data << std::endl;
	}

	AsyncTest::AsyncTest() {
		//futureFunc = std::async(std::launch::async, std::bind(&AsyncTest::testFutureFunc, this));
		futureFunc = boost::async(boost::launch::async, boost::bind(&AsyncTest::testFutureFunc, this));
		std::cout << "Inside AsyncTest constructor" << std::endl;

		data = 10;
	}


	void AsyncTest::WaitForThread() {
		futureFunc.wait();
	}
}
