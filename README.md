# Test Application for Mingw-w64 Cross Toolchain (Linux-to-Windows)

This project tests the said cross toolchain.
The projecta also cross compiles the source code into Windows executable. 
The project progresses from simple Helloworld application to increasingly complex application that cross-compiled.

## Current State

At the moment the code can cross compile and statically link C++ Helloworld application. 
Test in Windows 8.1 shows the executable works just fine.

## How to Build

Invoke cross_build.sh script from Arch Linux which has mingw-w64 cross toolchain.

## NOTE:
- build.sh and toolchain-xxx file is only remnants of old build system. 
  __DO NOT__ use them,   they have been superseded by cross_build.sh as 
  a more reliable build system.
