#pragma once

#include <boost/thread.hpp>
#include <boost/thread/future.hpp>

//#include <future>


namespace pcz {
	class AsyncTest {
		public:
			AsyncTest();
			void WaitForThread();

		private:
			//std::future<void> futureFunc;
			boost::future<void> futureFunc;
			void testFutureFunc();
			int data;
	};	
}
